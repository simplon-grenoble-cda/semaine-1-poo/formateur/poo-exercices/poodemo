package com.company;

public class ExcelFile extends File {
    protected int numberOfColumns;

    public ExcelFile(String fileName, int fileSize, int numberOfColumns) {
        super(fileName, fileSize);
        this.numberOfColumns = numberOfColumns;
    }

    public int getNumberOfColumns() {
        return numberOfColumns;
    }
}
