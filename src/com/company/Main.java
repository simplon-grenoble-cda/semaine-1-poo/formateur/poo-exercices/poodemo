package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        int myNumber = 10;
        Person myPerson = new Person("Alice", 20);

        Person myPerson2 = new Person();

        if (myPerson.hasMajority()) {
            System.out.println(myPerson.getName() + " can go into a nightclub");
        } else {
            System.out.println(myPerson.getName() + " can go to bed early");
        }

        File file1 = new File("/notes.txt", 20);
        File file2 = new File("/blabla.txt", 52);
        ExcelFile excelFile1 = new ExcelFile("/compta.xls", 60, 4);

        tellIfFileIsTooBig(file1);
        tellIfFileIsTooBig(file2);
        tellIfFileIsTooBig(excelFile1);
    }


    /**
     * Give a file as first paramater.
     * If the file size is greater than 50, it prints on the console
     * "file is too big"
     * @param file
     */
    public static void tellIfFileIsTooBig(File file) {
        if (file.getFileSize() > 50) {
            System.out.println("File is too big");
        }
    }

}
